# Deploy a GitLab Runner instance

To create a Runner instance:

1. Create a generic secret containing the key, `runner-registration-token`

    `kubectl create secret generic runner-token-secret --from-literal runner-registration-token="TOKEN_FROM_GITLAB_INSTANCE"`

2. Create a new Runner instance referencing the secret created in step 1 above

```
$ cat <<EOF>> example-runner.yaml
apiVersion: apps.gitlab.com/v1beta1
kind: Runner
metadata:
  name: example
spec:
  gitlab:
    url: https://gitlab.com
  token: runner-token-secret # Name of the secret containing the Runner token
  tags: openshift, test
EOF
```

3. Deploy the runner instance in your cluster using either `kubectl` or `oc`

`$ kubectl apply -f example-runner.yaml`

## Runner Spec Options
Below are some of the Runner configuration options exposed via the operator:

- The `tags` option can be used to filter which jobs can be processed by a given runner instance  

- The `token` contains the name of the secret containing the `runner-registration-token`  

- The `gitlab.url` and `gitlab.name` options are mutually exclusive and are used to identify the GitLab instance the Runner will register against. The `gitlab.name` can only be used if the GitLab instance was created via the operator and the Runner will co-exist within the same namespace / project.  

- The `concurrent` option limits how many jobs globally can be run concurrently. If not set, the operator configures this value to `10`  

- The `check_interval` set the time in seconds between job checks. If not set, the operator configures this value to `30`
