# GitLab Operator

The GitLab operator creates and manages GitLab and GitLab Runner instances in a container platform such as Openshift or Kubernetes.


## Requirements
The GitLab operator uses native kubernetes resources to deploy and manage GitLab in the environment. It therefore will run in any environment that provides deployments, statefulsets, services, ingress/openshift routes, persistent volume claims, persistent volumes, etc.

In addition to the Kubernetes resources, the GitLab operator relies on the following operators:
 * Cert Manager operator to provision certificates (Requires the 0.16.0 release of cert-manager or greater)
 * Nginx Inc. Ingress Operator to manage ingress resources

## GitLab Operator
The Gitlab Operator introduces three custom resource types at this time:

### 1. GitLab

```
apiVersion: apps.gitlab.com/v1beta1
kind: GitLab
metadata:
  name: example
spec:
  url: gitlab.example.com
  volume:
    capacity: 10Gi
  registry:
    url: registry.example.com
  objectStore:
    development: true
  redis:
    volume:
      capacity: 4Gi
  postgres:
    volume:
      capacity: 8Gi
  autoscaling:
    minReplicas: 2
    maxReplicas: 5
    targetCPU: 60
```

### 2. Runner

GitLab Runner is the open source project that is used to run your jobs and send the results back to GitLab. It is used in conjunction with GitLab CI/CD, the open-source continuous integration service included with GitLab that coordinates the jobs.

The GitLab operator only deploys runners with the Kubernetes executor.

Below is a sample runner instance for reference.

```
apiVersion: apps.gitlab.com/v1beta1
kind: Runner
metadata:
  name: example
spec:
  gitlab:
    url: https://gitlab.com
  token: runner-token-secret # Name of the secret containing the Runner token
  tags: openshift, test
```

More information on the runner spec [options](runner.md), can be found on the Runner page.

### 3. GLBackup

```
apiVersion: apps.gitlab.com/v1beta1
kind: GLBackup
metadata:
  name: example
spec:
  instance: example
  schedule: 30 1 1 * 6
  skip: repository,registry
```

## Clean up
The operator does not delete the persistent volume claims that hold the stateful data when a GitLab instance is deleted. Therefore, remember to delete any lingering volumes

In addition, when you delete or uninstall the operator from the environment, you will need to remove the CRDs.

```
$ make uninstall
```
